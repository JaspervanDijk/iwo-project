#!/bin/bash

# shows how mant tweest there are
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

# shows how many unique tweets there are
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l

# shows how many retweets there are from the uniquer tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -wi ^RT | wc-l

# The first 20 unique tweets that are not retweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -wiv ^RT | head -20
#


